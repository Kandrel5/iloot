﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using Newtonsoft.Json;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.Messages.SystemMessages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ILOOT
{
    public class ILOOT : ACorpseObserver
    {
        List<Rule> rules;
        Window wRules;
        string pDir;
        public override void Run(string pluginDir)
        {
            pDir = pluginDir;
            Chat.WriteLine("ILOOT - Type /iloot");

            LoadRules();

            InitCorpseListener();
            OnNewCorpse += Loot_OnNewCorpse;
            Inventory.ContainerOpened += OnContainerOpened;

            Network.N3MessageReceived += N3MessageReceived;
            Chat.RegisterCommand("iloot", OnUlootCommand);
            Game.OnUpdate += OnUpdate;
        }

        private void N3MessageReceived(object sender, N3Message n3Msg)
        {
            if (n3Msg.N3MessageType == N3MessageType.InventoryUpdate)
            {
                try
                {
                    corpseToLoot.TryRemove(((InventoryUpdateMessage)n3Msg).InventoryIdentity, out CorpseToLootTimers dummy);
                }
                catch { }
            }
        }

        private void LoadRules()
        {
            rules = new List<Rule>();

            string filename = Path.Combine(pDir, "ULOOT.json");
            if (File.Exists(filename))
            {
                string rulesJson = File.ReadAllText(filename);
                rules = JsonConvert.DeserializeObject<List<Rule>>(rulesJson);
                foreach (Rule r in rules)
                    r.Global = true;
            }


            filename = Path.Combine(pDir, DynelManager.LocalPlayer.Name + "_ULOOT.json");
            if (File.Exists(filename))
            {
                List<Rule> scopedules = new List<Rule>();
                string rulesJson = File.ReadAllText(filename);
                scopedules = JsonConvert.DeserializeObject<List<Rule>>(rulesJson);
                foreach (Rule r in scopedules)
                {
                    r.Global = false;
                    rules.Add(r);
                }

            }

            rules = rules.OrderBy(o => o.Name.ToUpper()).ToList();

        }

        private void SaveRules()
        {
            string filename = Path.Combine(pDir, "ULOOT.json");
            List<Rule> GlobalRules = new List<Rule>();
            List<Rule> ScopeRules = new List<Rule>();

            GlobalRules = rules.Where(o => o.Global == true).ToList();
            ScopeRules = rules.Where(o => o.Global == false).ToList();

            string rulesJson = JsonConvert.SerializeObject(GlobalRules);
            File.WriteAllText(filename, rulesJson);

            filename = Path.Combine(pDir, DynelManager.LocalPlayer.Name + "_ULOOT.json");
            rulesJson = JsonConvert.SerializeObject(ScopeRules);
            File.WriteAllText(filename, rulesJson);
        }

        bool LOOT = true;
        bool BAGS = false;


        public bool CheckRules(Item item)
        {
            foreach (Rule rule in rules)
            {
                if (
                    item.Name.ToUpper().Contains(rule.Name.ToUpper()) &&
                    item.QualityLevel >= Convert.ToInt32(rule.Lql) &&
                    item.QualityLevel <= Convert.ToInt32(rule.Hql)
                    )
                    return true;

            }
            return false;
        }


        private bool CheckRules(List<Item> Inventoryitems, Item item)
        {

            foreach (Rule rule in rules)
            {
                if (
                    item.Name.ToUpper().Contains(rule.Name.ToUpper()) &&
                    item.QualityLevel >= Convert.ToInt32(rule.Lql) &&
                    item.QualityLevel <= Convert.ToInt32(rule.Hql)
                    )
                {
                    if (rule.Unique)
                    {

                        foreach (Item iitem in Inventoryitems)
                        {
                            if (item.Name.ToUpper() == iitem.Name.ToUpper())
                                return false; //already in inventory

                        }
                        //Not in inventory
                        return true;
                        
                    }
                    else 
                    {

                        return true;
                    }
                
                
                }
                    //return false;

            }
            return false;

        }


        private void OnContainerOpened(object sender, Container e)
        {
            if (!LOOT) return;

            if (corpseToLoot.ContainsKey(e.Identity))
            {
                corpseToLoot.TryRemove(e.Identity, out CorpseToLootTimers dummy);
                //can apply filters here
                foreach (Item item in e.Items)
                {
                    if (CheckRules(Inventory.Items, item))
                    {
                        item.MoveToInventory();
                    }
                }

            }
            else
            {
                Corpse corpsedynel = DynelManager.Corpses.Where(o => o.Identity == e.Identity).FirstOrDefault();
                if (corpsedynel != null)
                {
                    foreach (Item item in e.Items)
                    {
                        if (CheckRules(Inventory.Items, item))
                        {
                            item.MoveToInventory();
                        }
                    }
                }
            }
        }

        

        ConcurrentDictionary<Identity, CorpseToLootTimers> corpseToLoot = new ConcurrentDictionary<Identity, CorpseToLootTimers>();
        private void Loot_OnNewCorpse(Corpse corpse)
        {
            corpseToLoot.TryAdd(corpse.Identity, new CorpseToLootTimers( Time.NormalTime + 300));
        }

        private void OnUlootCommand(string arg1, string[] arg2, ChatWindow arg3)
        {

            wRules = Window.CreateFromXml("ILoot", pDir + "\\UI\\rules.xml", new Rect(0, 0, 620, 430));

            wRules.FindView("ScrollListRoot", out MultiListView mlv);
            mlv.DeleteAllChildren();
            int iEntry = 0;
            foreach (Rule r in rules)
            {
                View entry = View.CreateFromXml(pDir + "\\UI\\ItemEntry.xml");
                entry.FindChild("ItemName", out TextView tx);

                //entry.Tag = iEntry;
                string scope = "";

                if (r.Global)
                    scope = "G";
                else
                    scope = "N";
                
                if (r.Unique)
                    tx.Text = (iEntry + 1).ToString() + " - " + scope + " - [" + r.Lql.PadLeft(3, ' ') + "-" + r.Hql.PadLeft(3, ' ') + "] - [U] - " + r.Name;
                else
                    tx.Text = (iEntry + 1).ToString() + " - " + scope + " - [" + r.Lql.PadLeft(3, ' ') + "-" + r.Hql.PadLeft(3, ' ') + "] - [*] - " + r.Name;


                mlv.AddChild(entry, false);
                iEntry++;
            }



            wRules.Show(true);

            wRules.FindView("chkOnOff", out Checkbox chkOnOff);
            chkOnOff.SetValue(LOOT);
            if (chkOnOff.Toggled == null)
                chkOnOff.Toggled += chkOnOff_Toggled;


            wRules.FindView("chkBags", out Checkbox chkBags);
            chkBags.SetValue(BAGS);
            if (chkBags.Toggled == null)
                chkBags.Toggled += chkBags_Toggled;


            wRules.FindView("buttonAdd", out Button addbut);
            if (addbut.Clicked == null)
                addbut.Clicked += addButtonClicked;


            wRules.FindView("buttonDel", out Button rembut);
            if (rembut.Clicked == null)
                rembut.Clicked += remButtonClicked;

            wRules.FindView("tivminql", out TextInputView tivminql);
            tivminql.Text = "1";
            wRules.FindView("tivmaxql", out TextInputView tivmaxql);
            tivmaxql.Text = "500";
        }

        private void chkBags_Toggled(object sender, bool e)
        {
            Checkbox chk = (Checkbox)sender;
            BAGS = e;
        }

        private void chkOnOff_Toggled(object sender, bool e)
        {
            Checkbox chk = (Checkbox)sender;
            LOOT = e;
        }

        private void addButtonClicked(object sender, ButtonBase e)
        {
            wRules.FindView("ScrollListRoot", out MultiListView mlv);

            wRules.FindView("tivName", out TextInputView tivname);
            wRules.FindView("tivminql", out TextInputView tivminql);
            wRules.FindView("tivmaxql", out TextInputView tivmaxql);

            wRules.FindView("tvErr", out TextView txErr);

            if (tivname.Text.Trim() == "")
            {
                txErr.Text = "Can't add an empty name";
                return;
            }

            int minql = 0;
            int maxql = 0;
            try
            {
                minql = Convert.ToInt32(tivminql.Text);
                maxql = Convert.ToInt32(tivmaxql.Text);
            }
            catch
            {
                txErr.Text = "Quality entries must be numbers!";
                return;
            }

            if (minql > maxql)
            {
                txErr.Text = "Min Quality must be less or equal than the high quality!";
                return;
            }
            if (minql <= 0)
            {
                txErr.Text = "Min Quality must be least 1!";
                return;
            }
            if (maxql > 500)
            {
                txErr.Text = "Max Quality must be 500!";
                return;
            }


            wRules.FindView("chkGlobal", out Checkbox chkGlobal);
            bool GlobalScope = chkGlobal.IsChecked;


            wRules.FindView("chkCond", out Checkbox chkCond);
            bool Unique = chkCond.IsChecked;



            mlv.DeleteAllChildren();

            Rule rule = new Rule(tivname.Text, tivminql.Text, tivmaxql.Text, GlobalScope, Unique);

            rules.Add(rule);

            rules = rules.OrderBy(o => o.Name.ToUpper()).ToList();

            int iEntry = 0;
            foreach (Rule r in rules)
            {
                View entry = View.CreateFromXml(pDir + "\\UI\\ItemEntry.xml");
                entry.FindChild("ItemName", out TextView tx);
                string globalscope = "";
                if (r.Global)
                {
                    globalscope = "[A]";
                }
                else
                    globalscope = "[N]";



                if (r.Unique)
                    tx.Text = (iEntry + 1).ToString() + " - " + globalscope + " - [" + r.Lql.PadLeft(3, ' ') + "-" + r.Hql.PadLeft(3, ' ') + "] - [U] - " + r.Name;
                else
                    tx.Text = (iEntry + 1).ToString() + " - " + globalscope + " - [" + r.Lql.PadLeft(3, ' ') + "-" + r.Hql.PadLeft(3, ' ') + "] - [*] - " + r.Name;



                if (r.Global)
                {
                    tx.Tag = r.EntryGuid;
                }


                mlv.AddChild(entry, false);
                iEntry++;
            }


            SaveRules();
            FileInfo fi = new FileInfo(Path.Combine(pDir, "ULOOT.json"));
            dtChanged = fi.LastWriteTimeUtc;
            LoadRules();


            tivname.Text = "";
            tivminql.Text = "1";
            tivmaxql.Text = "500";
            txErr.Text = "";
            chkCond.SetValue(false);

        }

        private void remButtonClicked(object sender, ButtonBase e)
        {
            try
            {
                wRules.FindView("ScrollListRoot", out MultiListView mlv);

                wRules.FindView("tivindex", out TextInputView txIndex);

                wRules.FindView("tvErr", out TextView txErr);

                if (txIndex.Text.Trim() == "")
                {
                    txErr.Text = "Cant remove an empty entry";
                    return;
                }

                int index = 0;

                try
                {
                    index = Convert.ToInt32(txIndex.Text) - 1;
                }
                catch
                {
                    txErr.Text = "Entry must be a number!";
                    return;
                }

                if (index < 0 || index >= rules.Count)
                {
                    txErr.Text = "Invalid entry!";
                    return;
                }

                Rule rule = rules[index];
                rules.RemoveAt(index);

                mlv.DeleteAllChildren();

                int iEntry = 0;
                foreach (Rule r in rules)
                {
                    View entry = View.CreateFromXml(pDir + "\\UI\\ItemEntry.xml");
                    entry.FindChild("ItemName", out TextView tx);

                    //entry.Tag = iEntry;

                    string scope = "";
                    if (r.Global)
                        scope = "[A]";
                    else
                        scope = "[N]";
                    
                    
                    if (r.Unique)
                        tx.Text = (iEntry + 1).ToString() + " - " + scope + " - [" + r.Lql.PadLeft(3, ' ') + "-" + r.Hql.PadLeft(3, ' ') + "] - [U] - " + r.Name;
                    else
                        tx.Text = (iEntry + 1).ToString() + " - " + scope + " - [" + r.Lql.PadLeft(3, ' ') + "-" + r.Hql.PadLeft(3, ' ') + "] - [*] - " + r.Name;


                    mlv.AddChild(entry, false);
                    iEntry++;
                }

                txErr.Text = "";
                SaveRules();
                FileInfo fi = new FileInfo(Path.Combine(pDir, "ULOOT.json"));
                dtChanged = fi.LastWriteTimeUtc;
                LoadRules();
            }
            catch (Exception ex)
            {

                Chat.WriteLine(ex.Message);
            }


        }


        DateTime dtChanged = new DateTime(0);
        private void OnUpdate(object sender, float e)
        {
            if (Game.IsZoning) return;

            try
            {
                if (wRules != null && wRules.IsValid && wRules.IsVisible)
                {
                    string filename = Path.Combine(pDir, "ULOOT.json");
                    if (File.Exists(filename))
                    {
                        FileInfo fi = new FileInfo(filename);

                        if (fi.LastWriteTimeUtc > dtChanged)
                        {
                            //Changed
                            LoadRules();

                            wRules.FindView("ScrollListRoot", out MultiListView mlv);
                            mlv.DeleteAllChildren();

                            int iEntry = 0;
                            foreach (Rule r in rules)
                            {
                                View entry = View.CreateFromXml(pDir + "\\UI\\ItemEntry.xml");
                                entry.FindChild("ItemName", out TextView tx);

                                //entry.Tag = iEntry;

                                string scope = "";
                                if (r.Global)
                                    scope = "[A]";
                                else
                                    scope = "[N]";
                                
                                if (r.Unique)
                                    tx.Text = (iEntry + 1).ToString() + " - " + scope + " - [" + r.Lql.PadLeft(3, ' ') + "-" + r.Hql.PadLeft(3, ' ') + "] - [U] - " + r.Name;
                                else
                                    tx.Text = (iEntry + 1).ToString() + " - " + scope + " - [" + r.Lql.PadLeft(3, ' ') + "-" + r.Hql.PadLeft(3, ' ') + "] - [*] - " + r.Name;

                                mlv.AddChild(entry, false);
                                iEntry++;
                            }




                        }
                    }

                }

            }
            catch { }

            CorpseListener();
            Parallel.ForEach(corpseToLoot.Keys,
            key =>
            {

                if (corpseToLoot[key].TTL < Time.NormalTime)
                {
                    corpseToLoot.TryRemove(key, out CorpseToLootTimers dummy);
                }
            });

         
            
        Parallel.ForEach(DynelManager.Corpses
                .Where(o => o.DistanceFrom(DynelManager.LocalPlayer) < 5f),
         corpse =>
         {
             {
                 try
                 {
                     if (corpse.IsValid && corpseToLoot.Keys.Contains(corpse.Identity))
                     {

                         CorpseToLootTimers ctlt = corpseToLoot[corpse.Identity];

                         if (ctlt.LASTTRYOPEN == 0 || Time.NormalTime - ctlt.LASTTRYOPEN > 1)
                         {
                             corpseToLoot[corpse.Identity].LASTTRYOPEN = Time.NormalTime;
                             corpse.Open();
                         }
                         



                     }
                     else
                     {
                         if (corpseToLoot.Keys.Contains(corpse.Identity))
                            corpseToLoot.TryRemove(corpse.Identity, out CorpseToLootTimers dummy);
                     }
                 }
                 catch
                 {
                     if (corpseToLoot.Keys.Contains(corpse.Identity))
                        corpseToLoot.TryRemove(corpse.Identity, out CorpseToLootTimers dummy);
                 }
             }
         });

            if (!LOOT) return;

            if (BAGS)
                foreach (Item it in Inventory.Items)
                {
                        if (it.Slot.Type == IdentityType.Inventory)
                            if (CheckRules(it))
                            {
                                //Genereting a key just to later check if the mov to inv were done
                                //MoveToContainer may fail and you just dont know
                                string key = it.Name + it.LowId.ToString() + it.HighId.ToString();
                                foreach (Backpack bck in Inventory.Backpacks)
                                {
                                    it.MoveToContainer(bck);
                                    foreach (Item itbck in bck.Items)
                                    {
                                    if (key == itbck.Name + itbck.LowId.ToString() + itbck.HighId.ToString())
                                        return; //3 for loops, return... the remaining items can be processed next interation
                                       
                                    }
                                }
                            }
                }

        }

        public override void Teardown()
        {
            base.Teardown();
        }


    }
}

