﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ILOOT
{
    public abstract class ACorpseObserver : AOPluginEntry
    {
        public delegate void delOnNewCorpse(Corpse corpse);

        private ConcurrentBag<Identity> LastCheckCorpses = new ConcurrentBag<Identity>();

        public event delOnNewCorpse OnNewCorpse;

        public void InitCorpseListener()
        {
            foreach (Corpse item in DynelManager.Corpses.OrderBy((Corpse o) => o.DistanceFrom(DynelManager.LocalPlayer)).ToList())
            {
                LastCheckCorpses.Add(item.Identity);
            }
        }

        public void CorpseListener()
        {
            List<Corpse> source = DynelManager.Corpses.OrderBy((Corpse o) => o.DistanceFrom(DynelManager.LocalPlayer)).ToList();
            Parallel.ForEach(source, delegate (Corpse corpse)
            {
                if (!LastCheckCorpses.Contains(corpse.Identity))
                {
                    this.OnNewCorpse(corpse);
                }
            });
            LastCheckCorpses = new ConcurrentBag<Identity>();
            Parallel.ForEach(source, delegate (Corpse corpse)
            {
                LastCheckCorpses.Add(corpse.Identity);
            });
        }
    }
}